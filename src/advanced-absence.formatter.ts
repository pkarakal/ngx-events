import { AdvancedColumnFormatter } from '@universis/tables';
import {ElementRef, Input} from '@angular/core';
import {TranslateService} from '@ngx-translate/core';

export class AbsenceFormatter extends AdvancedColumnFormatter {
    private _elementRef: ElementRef;
    render(data, type, row, meta) {
      let color, name;
      const column = meta.settings.aoColumns[meta.col];
      this._elementRef = this.injector.get(ElementRef);
      const translateService: TranslateService = this.injector.get(TranslateService);
      if (!row.hasOwnProperty('absent')) {
        row = {...row, absent: false};
      }
      if (column && column.data) {
        // Get this value from
        // The actual event
        if (row.presenceType == 0) {
          name = translateService.instant('Events.Absent');
          color = 'danger';
        } else {
          name = translateService.instant('Events.Present');
          color = 'info';
        }
        const toggle = `
          <script src="https://code.jquery.com/jquery-3.5.1.min.js"></script>
          <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/js/bootstrap.bundle.min.js"></script>
          <script>
          $(document).ready(function(){
              $("#myButtons .btn").click(function(){
                  $(this).button('toggle');
                  row.absent = !row.absent;
              });
          });
          </script>
              <span id="myButtons" style="float: right;">
                  <span class="btn-group btn-group-toggle" data-toggle="buttons">
                    <label class="btn btn btn-outline-${color}">
                    <input type="radio" name="options" id="option1" autocomplete="off" > ${name}
                    </label>
                    <label class="btn btn-outline-secondary active">
                    <input type="radio" name="options" id="option2" autocomplete="off" checked> &#10799;
                    </label>
                  </span>
              </span>
        `;
        return toggle;
      }
      return data;
    }
    constructor( ) {
      super();
    }
  }
