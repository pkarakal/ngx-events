import { Injectable } from '@angular/core';
import {AngularDataContext} from '@themost/angular';

export const enum Model {
  Instructor = 'instructors',
  Student = 'students'
}

@Injectable({
  providedIn: 'root'
})
export class EventsService {

  constructor(private _context: AngularDataContext) { }

  /**
   * This
   * @param model
   */
  async getUpcomingEvents(model: Model) {
    return this._context.model(model + '/me/teachingEvents')
      .asQueryable()
      .where('startDate')
      .expand('courseClass')
      .greaterOrEqual((new Date()).toISOString())
      .orderBy('startDate asc')
      .thenBy('courseClass/title')
      .take(3)
      .getItems();
  }
}
