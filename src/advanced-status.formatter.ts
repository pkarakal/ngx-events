import { AdvancedColumnFormatter } from '@universis/tables';
import {ConfigurationService} from '@universis/common';
import {TranslateService} from '@ngx-translate/core';

export class StatusFormatter extends AdvancedColumnFormatter {
    eventCompleted: any;
    eventOpen: any;
    render(data, type, row, meta) {
      const column = meta.settings.aoColumns[meta.col];
      const currLocale = this.injector.get(ConfigurationService).currentLocale;
      const translateService: TranslateService = this.injector.get(TranslateService);
      this.eventCompleted =  translateService.instant('Events.EventCompleted');
      this.eventOpen = translateService.instant('Events.EventOpened');
      if (column && column.data) {
        if (row.eventStatus === 'EventCompleted') {
            return `<span style="color:red">${this.eventCompleted}</span>`;
        } else {
            return `<span style="color:DeepSkyBlue">${this.eventOpen}</span>`;
        }
      }
      return data;
    }
    constructor( private configurationService: ConfigurationService ) {
      super();
    }
  }
