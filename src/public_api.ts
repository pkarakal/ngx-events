/*
 * Public API Surface of universis-events
 */

export * from './events.service';
export {EventsModule} from './events.module';
// export * from './advanced-table.formatters';
export * from './components/events/events.component';
export * from './components/events/events.configuration';
export * from './components/new-event/new-event.component';
export * from './components/absences/absences.component';
export * from './components/event-absences/event-absences.component';
export * from './components/upcoming-events/upcoming-events.component';
export * from './advanced-date.formatter';
export * from './advanced-absence.formatter';
export * from './advanced-status.formatter';
