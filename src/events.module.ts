import {CUSTOM_ELEMENTS_SCHEMA, NgModule} from '@angular/core';
import { TranslateModule, TranslateService } from '@ngx-translate/core';
import { TablesModule } from '@universis/tables';
import { SharedModule } from '@universis/common';
import {EventsLibComponent} from './components/events/events.component';
import {NewEventComponent} from './components/new-event/new-event.component';
import {FormsModule} from '@angular/forms';
import {CommonModule} from '@angular/common';
import {AdvancedFormsModule} from '@universis/forms';
import { AbsencesComponent } from './components/absences/absences.component';
import {ModalService} from '@universis/common';
import { environment } from './environments/environment';
import {EVENTS_LOCALES} from './components/i18n';
import { EventAbsencesComponent } from './components/event-absences/event-absences.component';
import {RouterModalModule} from '@universis/common/routing';
import {UpcomingEventsComponent} from './components/upcoming-events/upcoming-events.component';
import {RouterModule} from '@angular/router';
import { CalendarModule, DateAdapter } from 'angular-calendar';
import { adapterFactory } from 'angular-calendar/date-adapters/date-fns';
import {ModalModule, ProgressbarConfig, ProgressbarModule} from "ngx-bootstrap";

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    TranslateModule,
    TablesModule,
    SharedModule,
    AdvancedFormsModule,
    RouterModalModule,
    RouterModule,
    CalendarModule.forRoot({
      provide: DateAdapter,
      useFactory: adapterFactory
    }),
    ModalModule,
    ProgressbarModule
  ],
  declarations: [
    EventsLibComponent,
    NewEventComponent,
    AbsencesComponent,
    EventAbsencesComponent,
    UpcomingEventsComponent
  ],
  exports: [
    EventsLibComponent,
    NewEventComponent,
    UpcomingEventsComponent
  ],
  providers: [
    ModalService,
    ProgressbarConfig
  ],
  schemas: [
    CUSTOM_ELEMENTS_SCHEMA
  ],
  entryComponents: [
    NewEventComponent, EventsLibComponent, UpcomingEventsComponent
  ]
})
export class EventsModule {
  constructor( private _translateService: TranslateService) {
    this.ngOnInit();
}
  // tslint:disable-next-line:use-life-cycle-interface
  ngOnInit() {
    environment.languages.forEach( language => {
        if (EVENTS_LOCALES.hasOwnProperty(language)) {
            this._translateService.setTranslation(language, EVENTS_LOCALES[language], true);
        }
    });
  }
}
