import {Component, OnInit, Output, EventEmitter, ViewChild, Injector, Input} from '@angular/core';
import {EVENT_ABSENCES_CONFIGURATION} from './event-absences.configuration';
import {ErrorService, LoadingService, ModalService, ToastService} from '@universis/common';
import { AngularDataContext } from '@themost/angular';
import {AdvancedTableComponent, AdvancedTableDataResult, AdvancedTableEditorDirective} from '@universis/tables';
import {ActivatedRoute, Router} from '@angular/router';
import {combineLatest, Subscription} from 'rxjs';
import {TranslateService} from '@ngx-translate/core';

@Component({
  selector: 'universis-event-absences',
  templateUrl: './event-absences.component.html'
})
export class EventAbsencesComponent implements OnInit {
  private _router: Router;
  private _activatedRoute: ActivatedRoute;
  private _paramSubscription: Subscription;
  private _toastService: ToastService;
  private event;
  private status = [];
  public eventStatus;
  public tableConfiguration: any = EVENT_ABSENCES_CONFIGURATION;
  public filter: any = {};
  public recordsTotal: any;
  @Input() _model = '';
  @Input() eventModel = '';
  @ViewChild(AdvancedTableEditorDirective) tableEditor: AdvancedTableEditorDirective;
  @ViewChild('table') table: AdvancedTableComponent;

  @Output() refreshAction: EventEmitter<any> = new EventEmitter<any>();
  constructor(
    private _loadingService: LoadingService,
    private _modalService: ModalService,
    private _context: AngularDataContext,
    private _errorService: ErrorService,
    private _injector: Injector,
    private _translateService: TranslateService
  ) { }

  ngOnInit() {
    this._router = this._injector.get(Router);
    this._activatedRoute = this._injector.get(ActivatedRoute);
    this._toastService = this._injector.get(ToastService);
    this._paramSubscription = combineLatest(this._activatedRoute.params,
      this._activatedRoute.parent.params, this._activatedRoute.data).subscribe(x => {
      if (this._model === '') {
        if (x.length === 3 && x[2] && x[2]._model) {
          this._model = x[2]._model;
          this.eventModel = x[2].eventModel;
        }
      }

      Promise.all([this._context.model(this.eventModel)
        .where('id').equal(x[0].id).getItem(), this._context.model(this._model)
        .take(-1)
        .getItems() ]).then((values) => {
          this.event = values[0];
          this.eventStatus = (this.event.eventStatus.name === 'EventOpened');
          for (let i = 0; i < values[1].length; i++) { values[1][i]['presenceType'] = this.event.presenceType; }
          this.tableEditor.set(values[1]);
      });
    });
  }

  onDataLoad(data: AdvancedTableDataResult) {
    this.recordsTotal = data.recordsTotal;
  }


  async allPresent() {
    this._modalService.showWarningDialog(this._translateService.instant('Events.Warning'),
      this._translateService.instant('Events.AllPresentCheck'))
      .then( async result => {
      if (result === 'ok') {
        try {
          this._loadingService.showLoading();
          this.table.selectable = true;
          this.table.selectAny();
          if (this.event.hasOwnProperty('presenceType') && !this.event.presenceType) {
            await this._context.model(this._model)
              .save([]);
          } else if (this.event.hasOwnProperty('presenceType') && this.event.presenceType === 1) {
            const attendanceList = new Set();
            const selected = this.table.selected;
            for (const student of selected) {
              attendanceList.add(
                {
                  event: this.event.id,
                  student: student.student,
                  studentCourseClass: student.id
                }
              );
            }
            await this._context.model(this._model)
              .save(Array.from(attendanceList));
          }
          this._loadingService.hideLoading();
          this._toastService.show(this._translateService.instant('Events.ActionCompleted'),
            this._translateService.instant('Events.AllSavedPresent'),
            true, 20000);
        } catch (err) {
          console.log(err);
          this._loadingService.hideLoading();
          this._errorService.showError(err, {
            continueLink: '.'
          });
        }
      }
    });
  }

  async save() {
    this._loadingService.showLoading();
    try {
      this.status = [];
      let transl: string;
      const html = this.table.table.nativeElement.innerHTML;
      const buttons = html.split('btn-outline-secondary');
      this.table.selectable = true;
      this.table.selectAny();
      const selected = this.table.selected;
      const attendanceList = new Set();
      if (this.event.hasOwnProperty('presenceType') && !this.event.presenceType) {
        buttons.forEach(element => this.status.push(!(element.split('">', 1)[0] === ' active')));
        transl = 'Events.SavedAbsent';
      }
      if (this.event.hasOwnProperty('presenceType') && this.event.presenceType) {
        buttons.forEach(element => this.status.push((element.split('">', 1)[0] === ' active')));
        transl = 'Events.SavedPresent';
      }
      this.status.shift();
      let i = 0;
      for (const stat of this.status) {
        if (stat) {
          attendanceList.add(
            {
              event: this.event.id,
              student: selected[i].student,
              studentCourseClass: selected[i].id
            }
          );
        }
        i++;
      }
      await this._context.model(this._model)
        .save(Array.from(attendanceList));
      this._loadingService.hideLoading();
      this._toastService.show(this._translateService.instant('Events.ActionCompleted'),
        this._translateService.instant(transl), true, 20000);
    } catch (err) {
      console.log(err);
      this._loadingService.hideLoading();
      this._errorService.showError(err, {
        continueLink: '.'
      });
    }
  }

  async commit() {
    this._modalService.showWarningDialog(this._translateService.instant('Events.Warning'),
      this._translateService.instant('Events.CommitCheck'))
      .then( async result => {
        if(result === 'ok') {
          this._loadingService.showLoading();
          try {
            await this._context.model(this._model.replace('attendance', 'close')).save([]);
            this._loadingService.hideLoading();
            this._toastService.show(this._translateService.instant('Events.ActionCompleted'),
              this._translateService.instant('Events.MarkedCompleted'), true, 20000);
            this._router.navigate(['../../']);
          } catch (err) {
            console.log(err);
            this._loadingService.hideLoading();
            this._errorService.showError(err, {
              continueLink: '.'
            });
          }
        }
     });

  }
}
