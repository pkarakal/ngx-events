export const EVENT_ABSENCES_CONFIGURATION = {
  "title": "StudentPeriodRegistrations.Courses",
  "model": "StudentCourseClasses",
  "searchExpression":"indexof(student/familyName, '${text}') ge 0 or indexof(student/givenName, '${text}') ge 0 or indexof(student/studentIdentifier, '${text}') ge 0",
  "selectable": false,
  "multipleSelect": true,
  "columns": [
    {
      "name": "id",
      "property": "id",
      "hidden": true
    },

    {
      "name":"student/familyName",
      "property": "familyName",
      "title":"Last Name"
    },
    {
      "name":"student/givenName",
      "property": "givenName",
      "title":"Student Name"
    },
    {
      "name":"student/fatherName",
      "property": "fatherName",
      "title":"Father Name"
    },
    {
      "name":"student/studentIdentifier",
      "property": "studentIdentifier",
      "title":"Student Identifier"
    },
    {
      "name":"student/registrationSemester",
      "property": "registrationSemester",
      "title":"Semester"
    },
    {
      "name":"studentCourseClass",
      "property": "studentCourseClass",
      "title":"studentCourseClass",
      "hidden": true
    },
    {
      "name":"attendances",
      // "title":"Attendances",
      "formatter": "AbsenceFormatter",
      "hidden": false
    }
  ],
  'criteria': [
    ],
  'searches': [  ],
  'paths' : [  ]
  // "defaults":{
  //   "orderBy": "semester asc,courseClass/title asc"
  // },
//  "paths": [

  // ],
  // "criteria" : [
  //   {
  //     "name": "semester",
  //     "filter": "(semester/name eq '${value}')",
  //     "type": "text"
  //   },
  //   {
  //     "name": "name",
  //     "filter": "(indexof(courseClass/title, '${value}') ge 0)",
  //     "type": "text"
  //   },
  //   {
  //     "name": "displayCode",
  //     "filter": "(indexof(course/displayCode, '${value}') ge 0)",
  //     "type": "text"
  //   },
  //   {
  //     "name": "courseSector",
  //     "filter": "(course/courseSector eq '${value}')",
  //     "type": "text"
  //   },
  //   {
  //     "name": "courseTypeName",
  //     "filter": "(course/courseType/course eq '${value}')",
  //     "type": "text"
  //   }
  //]
};
