import {Component, Injector, OnInit, Input, ViewChild, OnDestroy} from '@angular/core';
import {AngularDataContext} from '@themost/angular';
import {ActivatedRoute, Router} from '@angular/router';
import {Observable, Subscription} from 'rxjs';
import {
  AppEventService,
  ErrorService,
  LoadingService,
  ModalService,
  ToastService,
  UserActivityService
} from '@universis/common';
import {ABSENCES_CONFIGURATION} from './absences.configuration';
import {TranslateService} from '@ngx-translate/core';
import {AdvancedTableComponent, AdvancedTableEditorDirective, AdvancedSearchFormComponent} from '@universis/tables';
import {AddItemsComponent} from '@universis/tables';

@Component({
  selector: 'universis-absences',
  templateUrl: './absences.component.html'
})
export class AbsencesComponent implements OnInit, OnDestroy {

  @ViewChild('classes') classes: AdvancedTableComponent;
  @ViewChild('search') search: AdvancedSearchFormComponent;
  @ViewChild(AdvancedTableEditorDirective) tableEditor: AdvancedTableEditorDirective;
  private dataSubscription: Subscription;
  private paramSubscription: Subscription;
  private fragmentSubscription: Subscription;
  private reloadSubscription: Subscription;
  private _router: Router;
  private _activatedRoute: ActivatedRoute;

  public isCurrentRegistration = false;
  public loading = true;
  public recordsTotal: any;
  public registration: any;

  constructor(
              // private _activatedRoute: ActivatedRoute,
              private _context: AngularDataContext,
              private _translateService: TranslateService,
              private _userActivityService: UserActivityService,
              // private _router: Router,
              private _errorService: ErrorService,
              private _modalService: ModalService,
              private _toastService: ToastService,
              private _loadingService: LoadingService,
              private _appEvent: AppEventService,
              private _injector: Injector) {
  }

  ngOnInit() {
    this._router = this._injector.get(Router);
    this._activatedRoute = this._injector.get(ActivatedRoute);
    this.paramSubscription = this._activatedRoute.paramMap.subscribe(x => {

    });
  }
  async add() {
    try {
      const tableConfiguration =  ABSENCES_CONFIGURATION;
      tableConfiguration.model = `instructors/me/classes/600174971/teachingEvents/959/attendance`;
      // const courseTypes = await this._context.model(tableConfiguration.model)
      //   .getItems();

      this._modalService.openModalComponent(AddItemsComponent, {
        class: 'modal-xl modal-table',
        keyboard: false,
        ignoreBackdropClick: true,
        initialState: {
          tableConfiguration: tableConfiguration,
          target: this.registration,
          execute: (() => {
            return new Observable((observer) => {
              try {
                // get add courses component
                const component = <AddItemsComponent>this._modalService.modalRef.content;
                const courseClasses = [];
                const courseClassesFromTable = this.tableEditor.rows();

                component.items.forEach((item, index) => {
                  // @ts-ignore
                  const degree = courseClassesFromTable.find(e => e.courseClass === item.courseClass);
                  if (!degree) {
                    courseClasses.push(item);
                  }
                });

                this.tableEditor.add(...courseClasses);
                observer.next();
              } catch (e) {
                observer.error(e);
              }
            });
          })()
        }
      });
    } catch (err) {
      if (this._modalService.modalRef) {
        this._modalService.modalRef.hide();
      }
      this._errorService.showError(err, {
        continueLink: '.'
      });
    }
  }

  ngOnDestroy() {
    if (this.paramSubscription && !this.paramSubscription.closed) {
      this.dataSubscription.unsubscribe();
    }
  }
}

