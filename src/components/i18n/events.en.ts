/* tslint:disable quotemark */
/* tslint:disable max-line-length */
export const en = {
    "Date": "Date",
    "Time": "Time",
    "Name": "Event Name",
    "Last Name": "Last Name",
    "Student Name": "Student Name",
    "Father Name": "Father Name",
    "Student Identifier": "Student Identifier",
    "Semester": "Semester",
    "Attendances": "Attendances",
    "Description": "Description",
    "Status": "Status",
    "Events": {
      "UpcomingEvents": "Upcoming Events",
      "show": "Show",
      "newSimpleEvent": "New event",
      "newRecursiveEvent": "New recursive event",
      "AllPresent": "All present",
      "Save": "Save",
      "Commit": "Submit",
      "Back": "Back",
      "CloseEvent": "Close Event",
      "OpenEvent": "Open Event",
      "ActionCompleted": "Action Completed",
      "MarkedCompleted": "Marked current event as completed",
      "AllSavedPresent": "Marked and saved all students as present",
      "CreatedNew": "New Event",
      "CreatedSuccess": "New event successfully created",
      "Warning": "Warning",
      "AllPresentCheck": "This action saves all students in this event as present. Are you sure you want to proceed?",
      "SavedPresent": "Present students saved",
      "SavedAbsent": "Absent students saved",
      "Absent": "Absent",
      "Present": "Present",
      "CommitCheck": "This action cannot be reversed. Are you sure you want to proceed?",
      "Month": "Month",
      "Week": "Week",
      "Day": "Day",
      "This": {
        "Day": "Today",
        "Month": "This month",
        "Week": "This week"
      },
      "Next": {
        "Day": "Tomorrow",
        "Month": "Next month",
        "Week": "Next week"
      },
      "Previous": {
        "Day": "Yesterday",
        "Month": "Previous month",
        "Week": "Previous week"
      },
      "Export": "Export",
      "EventCompleted": "Completed event",
      "EventOpened" : "Open event",
      "OpenAction": {
        "Title": "Open teaching event",
        "Description": "This action is going to change the status of the selected teaching events to open. An open teaching event can be modified by the instructors and it will be visible both in the students' and the instructors' calendar."
      },
      "CloseAction": {
        "Title": "Close teaching event",
        "Description": "This action is going to change the status of the selected teaching events to completed. A completed teaching event cannot be modified by the instructors and they won't be able to add attendance records for that event."
      }
    },

};
/* tslint:enable max-line-length */
/* tslint:enable quotemark */
