import {
  ChangeDetectionStrategy,
  Component,
  EventEmitter,
  Injector,
  Input,
  OnInit,
  Output,
  ViewChild, ViewEncapsulation
} from '@angular/core';
import {EVENTS_CONFIGURATION} from './events.configuration';
import {ConfigurationService, ErrorService, LoadingService, ModalService} from '@universis/common';
import {NewEventComponent} from '../new-event/new-event.component';
import {AngularDataContext} from '@themost/angular';
import {Observable, Subject, Subscription} from 'rxjs';
import * as moment_ from 'moment';
import {DatePipe} from '@angular/common';
import {ActivatedRoute, Router} from '@angular/router';
import {AdvancedRowActionComponent, AdvancedTableComponent} from '@universis/tables';
import {ClientDataQueryable, ResponseError} from '@themost/client';
import {CalendarEvent, CalendarView, } from 'angular-calendar';
import {DateFormatter} from '../../advanced-date.formatter';
import {isSameDay, isSameMonth} from 'date-fns';
import {cloneDeep} from 'lodash';

import {TranslateService} from '@ngx-translate/core';

const moment = moment_;

const enum NextTranslate  {
  Month = 'Events.Next.Month',
  Week = 'Events.Next.Week',
  Day = 'Events.Next.Day'
}

const enum PreviousTranslate {
  Month = 'Events.Previous.Month',
  Week = 'Events.Previous.Week',
  Day = 'Events.Previous.Day'
}

const enum TodayTranslate {
  Month = 'Events.This.Month',
  Week = 'Events.This.Week',
  Day = 'Events.This.Day'
}

const MAX_EXPORT_ITEMS = 500;

@Component({
  selector: 'universis-events',
  templateUrl: './events.component.html'
})
export class EventsLibComponent implements OnInit {
  public tableConfiguration: any = EVENTS_CONFIGURATION;
  private _activatedRoute: ActivatedRoute;
  private _paramSubscription: Subscription;
  public filter: any = {};
  @Input() model = '';
  @Input() newModel = '';
  @Input() courseClass;
  @Input() instructor;
  @Input() sectionEndpoint = '';
  @Input() showActions = false;
  @Input() isAllowed = true;
  @Input() actionModel = '';
  @ViewChild('table') table: AdvancedTableComponent;
  @Output() refreshAction: EventEmitter<any> = new EventEmitter<any>();
  public tableView = true;
  public events: Array<CalendarEvent> = [];
  public view: CalendarView = CalendarView.Month;
  public CalendarView = CalendarView;
  public viewDate: Date = new Date();
  public activeDayIsOpen = false;
  private router: Router;
  private _configService: ConfigurationService;
  public locale: string;
  public nextTranslation: string = NextTranslate.Month;
  public previousTranslation: string = PreviousTranslate.Month;
  public todayTranslation: string = TodayTranslate.Month;
  newEventSubject: Subject<any> = new Subject();
  private selectedItems: Array<any>;

  constructor(
    private _loadingService: LoadingService,
    private _modalService: ModalService,
    private _context: AngularDataContext,
    private _errorService: ErrorService,
    private _injector: Injector
  ) { }

  ngOnInit() {
    this._activatedRoute = this._injector.get(ActivatedRoute);
    this.router = this._injector.get(Router);
    this._configService = this._injector.get(ConfigurationService);
    this.locale = this._configService.currentLocale;
    const refilter = new RegExp('\\?\\$filter=.*');
    this._paramSubscription = this._activatedRoute.data.subscribe(x => {
      if (x.model) {
        this.tableConfiguration = cloneDeep(EVENTS_CONFIGURATION);
        this.newModel = !this.newModel ? x.newModel : this.newModel;
        this.model = !this.model ? x.model : this.model;
        this.actionModel = !this.actionModel ? x.actionModel : this.actionModel;
        const res = refilter.exec(this.model)
        if(res){
          if (this.tableConfiguration.hasOwnProperty('defaults') && this.tableConfiguration.defaults.hasOwnProperty('filter')){
            res.forEach(x => {
              this.model = this.model.replace(x, "");
              this.tableConfiguration.defaults.filter += ` and (${x.split('=')[1]})`;
            });
          }
        }
        this.showActions = !this.showActions ? x.showActions : this.showActions;
        this.courseClass = !this.courseClass ? x.courseClass : this.courseClass;
        this.instructor = !this.instructor ? x.instructor : this.instructor;
        this.sectionEndpoint = !this.sectionEndpoint ? x.sectionEndpoint : this.sectionEndpoint;
        this.tableConfiguration.model = this.model;
        if(!this.isAllowed) {
          this.tableConfiguration.columns = this.tableConfiguration.columns.filter(x => x.name !== 'id' && x.formatter !== 'ButtonFormatter');
        }
      }
      this.table.ngOnInit();
    });
    if (!this.tableView) {
      this.createCalendarView().then(() => Promise.resolve());
    }
  }

  async newSimpleEvent() {
    try {
      this._modalService.openModalComponent(NewEventComponent, {
        class: 'modal-lg',
        keyboard: false,
        ignoreBackdropClick: true,
        initialState: {
          modalTitle: 'New event',
          formEditName: 'Events/newSimple',
          input: {...this.courseClass, endpoint: this.sectionEndpoint},
          execute:  (() => {
            return new Observable((observer) => {
              this._loadingService.showLoading();
              const component = <NewEventComponent>this._modalService.modalRef.content;
              let submissionData = component.formComponent.form.formio.data;
              submissionData =  this.parseFormData(submissionData, false);
              // and submit
              Promise.resolve(submissionData).then(res => {
                this._context.model(this.newModel).save(res).then(async() => {
                  this._loadingService.hideLoading();
                  observer.next();
                  this.ngOnInit();
                }).catch((err) => {
                  this._loadingService.hideLoading();
                  observer.error(err);
                });
              }).catch(err => {
                this._loadingService.hideLoading();
                observer.error(err);
                console.log(err);
              });
            });
          })()
        }

      });
    } catch (err) {
      console.log(err);
      this._errorService.showError(err, {
        continueLink: '.'
      });
    }
  }

  async newRecursiveEvent() {
    try {
      this._modalService.openModalComponent(NewEventComponent, {
        class: 'modal-lg',
        keyboard: false,
        ignoreBackdropClick: true,
        initialState: {
          modalTitle: 'New event',
          formEditName: 'Events/newRecursive',
          input: {...this.courseClass, endpoint: this.sectionEndpoint},
          execute:  (() => {
            return new Observable((observer) => {
              this._loadingService.showLoading();
              const component = <NewEventComponent>this._modalService.modalRef.content;
              let submissionData = component.formComponent.form.formio.data;
              submissionData =  submissionData =  this.parseFormData(submissionData, true);
              // and submit
              Promise.resolve(submissionData).then(res => {
                this._context.model(this.newModel).save(res).then(async() => {
                  this._loadingService.hideLoading();
                  observer.next();
                  this.ngOnInit();
                }).catch((err) => {
                  this._loadingService.hideLoading();
                  observer.error(err);
                });
              }).catch(err => {
                this._loadingService.hideLoading();
                observer.error(err);
                console.log(err);
              });
            });
          })()
        }

      });
    } catch (err) {
      console.log(err);
      this._errorService.showError(err, {
        continueLink: '.'
      });
    }
  }

  parseFormData(submissionData, isRecursive: boolean = false) {
    const datePipe: DatePipe = new DatePipe(this.locale);
    submissionData['about'] = this.courseClass && this.courseClass['id'];
    submissionData['courseClass'] = this.courseClass && this.courseClass.hasOwnProperty('id') && this.courseClass['id'];
    submissionData['organizer'] = this.courseClass && this.courseClass.hasOwnProperty('course') &&
      this.courseClass['course'].hasOwnProperty('department') &&
      this.courseClass['course']['department'];
    submissionData['performer'] = this.instructor && this.instructor.hasOwnProperty('user') && this.instructor['user'];
    submissionData['inLanguage'] = null;
    if (isRecursive) {
      const duration = moment.duration(moment(submissionData.endTime).diff(moment(submissionData.startTime))).toISOString();
      const start = parseInt(datePipe.transform(submissionData.startDate, 'M'), 10);
      const end = parseInt(datePipe.transform(submissionData.endDate, 'M'), 10);
      const months = [];
      for (let i = start; i <= end; i += 1) {
        months.push(i);
      }
      const _months = months.join(',');
      const days = submissionData.pickDay.join(',');
      const validFrom = submissionData.startDate.split('T')[0] + 'T' + submissionData.startTime.split('T')[1];
      const validThrough = submissionData.endDate.split('T')[0] + 'T' + submissionData.endTime.split('T')[1];
      submissionData['eventHoursSpecification'] = {
        'duration': duration,
        'validFrom': validFrom,
        'validThrough': validThrough,
        'opens': datePipe.transform(submissionData.startTime, 'HH:mm:ss'),
        'closes': datePipe.transform(submissionData.endTime, 'HH:mm:ss'),
        'minuteOfHour': datePipe.transform(submissionData.startTime, 'mm'),
        'hourOfDay': datePipe.transform(submissionData.startTime, 'HH'),
        'dayOfMonth': '*',
        'monthOfYear': _months,
        'dayOfWeek': days
      };
      submissionData['duration'] = duration;
      submissionData['contributor'] = null;
      submissionData['isAccessibleForFree'] = true;
      submissionData['contributor'] = null;
      delete submissionData.pickDay;
    } else {
      submissionData['eventHoursSpecification'] = null;
      submissionData['duration'] = null;
      submissionData['contributor'] = null;
      submissionData['isAccessibleForFree'] = true;
      submissionData['contributor'] = null;
      submissionData['startDate'] =  submissionData.startDate.split('T')[0] + 'T' + submissionData.startTime.split('T')[1];
      submissionData['endDate'] = submissionData.startDate.split('T')[0] + 'T' + submissionData.endTime.split('T')[1];
      delete submissionData.input;
      delete submissionData.startTime;
      delete submissionData.endTime;
    }
    submissionData =  [submissionData];
    return submissionData;
  }

  async getSelectedItems() {
    let items = [];
    if (this.table && this.table.lastQuery) {
      const lastQuery: ClientDataQueryable = this.table.lastQuery;
      if (lastQuery != null) {
        if (this.table.smartSelect) {
          // get items
          const selectArguments = ['id', 'eventStatus/alternateName as eventStatus'];
          // query items
          const queryItems = await lastQuery.select.apply(lastQuery, selectArguments)
            .expand('attendanceList')
            .take(-1)
            .skip(0)
            .getItems();
          items = queryItems.map(x => {
            if (this.table.selected.findIndex(item => {
              return x.id === item.id;
            }) > -1) {
              return {
                id: x.id,
                status: x.eventStatus,
                total: x.attendanceList.length
              };
            }
          }).filter(x => x);
        } else {
          // get selected items only
          items = this.table.selected.map((item) => {
            return {
              id: item.id,
              name: item.name,
              eventStatus: item.eventStatus
            };
          });
        }
      }
    }
    return items;
  }

  async openEventAction() {
    try {
      this._loadingService.showLoading();
      const items = await this.getSelectedItems();
      // get only items that may be opened
      this.selectedItems = items.filter( (item) => {
        return item.eventStatus !== "EventOpened";
      });
      this._loadingService.hideLoading();
      this._modalService.openModalComponent(AdvancedRowActionComponent, {
        class: 'modal-lg',
        keyboard: false,
        ignoreBackdropClick: true,
        initialState: {
          items: this.selectedItems,
          modalTitle: 'Events.OpenAction.Title',
          description: 'Events.OpenAction.Description',
          refresh: this.refreshAction,
          execute: this.executeChangeStatusAction('EventOpened', 'open')
        }
      });
    } catch (err) {
      console.log(err)
      this._loadingService.hideLoading();
      this._errorService.showError(err, {
        continueLink: '.'
      });
    }
  }

  private executeChangeStatusAction(status: string, action: string) {
    return new Observable((observer) => {
      this.refreshAction.emit({
        progress: 1
      });
      const total = this.selectedItems.length;
      const result = {
        total: total,
        success: 0,
        errors: 0
      };
      // execute promises in series within an async method
      (async () => {
        for (let index = 0; index < this.selectedItems.length; index++) {
          try {
            const item = this.selectedItems[index];
            // set progress
            this.refreshAction.emit({
              progress: Math.floor(((index + 1) / total) * 100)
            });
            // set active item
            const updated = {
              id: item.id,
              actionStatus: {
                alternateName: status
              }
            };
            await this._context.model(this.actionModel + `/${updated.id}/${action}`).save(updated);
            result.success += 1;
            // do not throw error while updating row
            // (user may refresh view)
            try {
              await this.table.fetchOne({
                id: updated.id
              });
            } catch (err) {
              //
            }

          } catch (err) {
            // log error
            console.log(err);
            result.errors += 1;
          }

        }
      })().then(() => {
        observer.next(result);
      }).catch((err) => {
        observer.error(err);
      });
    });  }

  async closeAction() {
    try {
      this._loadingService.showLoading();
      const items = await this.getSelectedItems();
      // get only items that may be opened
      this.selectedItems = items.filter( (item) => {
        return item.eventStatus !== "EventCompleted";
      });
      this._loadingService.hideLoading();
      this._modalService.openModalComponent(AdvancedRowActionComponent, {
        class: 'modal-lg',
        keyboard: false,
        ignoreBackdropClick: true,
        initialState: {
          items: this.selectedItems,
          modalTitle: 'Events.CloseAction.Title',
          description: 'Events.CloseAction.Description',
          refresh: this.refreshAction,
          execute: this.executeChangeStatusAction('EventCompleted', 'close')
        }
      });
    } catch (err) {
      console.log(err)
      this._loadingService.hideLoading();
      this._errorService.showError(err, {
        continueLink: '.'
      });
    }
  }

  copyAction() {

  }

  async createCalendarView() {
    const events = await this.table.lastQuery.getItems();
    if (this.isAllowed){
      this.events = events.map(x => {
        return {
          id: x.id,
          title: x.name,
          start: x.startDate,
          end: x.endDate,
          allDay: false,
          color: {
            primary: DateFormatter.stringToColour(x.name),
            secondary: DateFormatter.stringToColour(x.name)
          },
          actions: [
            {
              label: '<i class="fas fa-eye"></i>',
              a11yLabel: 'Preview',
              onClick: ({ event }: { event: CalendarEvent }): void => {
                this.navigateToEvent(event);
              },
            },
          ]
        };
      });
    } else {
      this.events = events.map(x => {
        return {
          title: x.name,
          start: x.startDate,
          end: x.endDate,
          allDay: false,
          color: {
            primary: DateFormatter.stringToColour(x.name),
            secondary: DateFormatter.stringToColour(x.name)
          }
        };
      });
    }
    this.newEventSubject.next();
  }

  public navigateToEvent(event: CalendarEvent<any>) {
    this.router.navigate([event.id, 'attendance'], {
      relativeTo: this._activatedRoute,
      replaceUrl: true
    });
  }

  setView(view: CalendarView) {
    this.view = view;
    switch (view) {
      case CalendarView.Day:
        this.previousTranslation = PreviousTranslate.Day;
        this.nextTranslation = NextTranslate.Day;
        this.todayTranslation = TodayTranslate.Day;
        break;
      case CalendarView.Week:
        this.previousTranslation = PreviousTranslate.Week;
        this.nextTranslation = NextTranslate.Week;
        this.todayTranslation = TodayTranslate.Week;
        break;
      case CalendarView.Month:
      default:
        this.previousTranslation = PreviousTranslate.Month;
        this.nextTranslation = NextTranslate.Month;
        this.todayTranslation = TodayTranslate.Month;
        break;
    }
  }

  dayClicked({ date, events }: { date: Date; events: CalendarEvent[] }): void {
    if (isSameMonth(date, this.viewDate)) {
      this.activeDayIsOpen = !((isSameDay(this.viewDate, date) && this.activeDayIsOpen === true) ||
        events.length === 0);
      this.viewDate = date;
    }
  }

  exportCalendar() {
    const lastQuery = this.table.lastQuery;
    const tableConfig = this.table.config;
    const serviceHeaders = this._context.getService().getHeaders();
    let query = this._context.model(lastQuery.getModel()).asQueryable();
    query = this.constructQuery(query, lastQuery.getParams(), tableConfig, serviceHeaders);
    const maxExportItems = (this._configService.settings.app as any).maxExportItems;
    if (maxExportItems && typeof maxExportItems === 'number' && maxExportItems > 0) {
      query.skip(0).take(maxExportItems);
    } else {
      query.skip(0).take(MAX_EXPORT_ITEMS);
    }
    // This constructs the URI based on the way @themost does
    const url = this.constructURL(query);
    this.downloadFile(url, query.getService().getHeaders(), query.getModel());
  }

  /**
   * Sets the parameters to the query based on
   * the active filters of the activated table
   * @param {ClientDataQueryable} query
   * @param params: The filters of the last query of the activated table
   * @return ClientDataQueryable
   */
  setParams(query: ClientDataQueryable, params): ClientDataQueryable {
    if (params && typeof params === 'object') {
      Object.keys(params).forEach((key) => {
        query.setParam(key, params[key]);
      });
    }
    return query;
  }

  /**
   * Sets the headers to the query based on
   * the default headers of the DataContext
   * and appending the accept header to get
   * an ics file from the API
   * @param {ClientDataQueryable} query
   * @param {Headers} headers: The headers to append to the query
   * @return ClientDataQueryable
   */
  setHeaders(query: ClientDataQueryable, headers: Headers): ClientDataQueryable {
    Object.keys(headers).forEach((key) => {
      if (headers.hasOwnProperty(key)) {
        query.getService().setHeader(key, headers[key]);
      }
    });
    // sets a header to accept ics files
    query.getService().setHeader('Accept', 'text/calendar');
    return query;
  }

  /**
   * Constructs the fetch URL based the query parameters
   * @param {ClientDataQueryable} query
   * @return String
   */
  constructURL(query): String {
    const baseURL = query.getService().getBase();
    let url = baseURL.endsWith('/') ? (baseURL + query.getModel() + '?') : (baseURL + '/' + query.getModel() + '?');
    Object.keys(query.getParams()).forEach((key, index) => {
      if (index === 0) {
        url += key + '=' + query.getParams()[key];
      } else {
        url += '&' + key + '=' + query.getParams()[key];
      }
    });
    return url;
  }

  /**
   * Constructs the query by setting the parameters of the
   * advanced table's active filters, sets the necessary headers
   * and build the select parameter of the query
   * @param {ClientDataQueryable} query
   * @param params: The filters of the last query of the activated table
   * @param tableConfig: The configuration of the activated table
   * @param {Headers} headers: The headers to append to the query
   * @return ClientDataQueryable
   */
  constructQuery(query, params, tableConfig, headers: Headers) {
    let _query = this.setParams(query, params);
    _query = this.setHeaders(_query, headers);
    return _query;
  }

  /**
   * Fetches and saves the xlsx file
   * @param {String} url
   * @param {Headers} serviceHeaders: The headers to append to the query
   * @param modelName: The model name of the advanced table
   */
  downloadFile(url, serviceHeaders: Headers, modelName) {
    this._loadingService.showLoading();
    fetch(encodeURI(url), {
      headers: serviceHeaders,
      credentials: 'include'
    }).then(response => {
      if (response && !response.ok) {
        throw new ResponseError(response.statusText, response.status);
      }
      return response.blob();
    }).then(blob => {
      const objectUrl = window.URL.createObjectURL(blob);
      const a = document.createElement('a');
      document.body.appendChild(a);
      a.setAttribute('style', 'display: none');
      a.href = objectUrl;
      const downloadName = `${modelName}.ics`;
      a.download = downloadName;
      // this adds support for IE and MS Edge (up to version 44.19041)
      if (window.navigator && window.navigator.msSaveOrOpenBlob) {
        window.navigator.msSaveOrOpenBlob(blob, downloadName);
      } else {
        // for all other browsers
        a.click();
      }
      window.URL.revokeObjectURL(objectUrl);
      a.remove();
      this._loadingService.hideLoading();
    }).catch(err => {
      this._loadingService.hideLoading();
      this._errorService.showError(err, {
        continueLink: '.'
      });
    });
  }


}
