import { TrueFalseFormatter } from "@universis/tables";

export const EVENTS_CONFIGURATION = {
  'title': 'Events',
  "settings": {
    "i18n": {
      "el": {
      }
    }
  },
  'model': 'instructors/me/teachingEvents',
  'searchExpression': '(indexof(name, \'${text}\') ge 0) or (day(startDate) eq ${/^\\d+$/.test(text) ? text : 0})',
  'selectable': false,
  'smartSelect': false,
  'defaults':{
    'orderBy': 'startDate desc',
    'filter': '((superEvent ne null) or (eventHoursSpecification eq null))'
  },
  'multipleSelect': true,
  'columns': [

    {
      'name': 'startDate',
      'property': 'startDate',
      'formatter': 'DateFormatter',
      'label': 'Date',
      'title': 'Date'
    },
    {
      'name': 'endDate',
      'property': 'endDate',
      'hidden': true
    },
    {
      'name': 'courseClass/title',
      'property': 'courseClass',
      'title': 'Class',
      'label': 'Class',
      'hidden': true
    },
    {
      'name': 'name',
      'property': 'name',
      'title': 'Name',
      'label': 'Name',
      'hidden': false
    },
    {
      'name': 'description',
      'property': 'description',
      'title': 'Description',
      'label': 'Description',
      'hidden': false
    },
    {
      'name': 'eventStatus/name',
      'property': 'eventStatus',
      'formatter': 'StatusFormatter',
      'title': 'Status',
      'label': 'Status',
      'hidden': false
    },
    {
      'name': 'id',
          'formatter': 'ButtonFormatter',
          'className': 'text-center',
          'formatOptions': {
            'buttonContent': '<i class="far fa-eye text-indigo"></i>',
            'buttonClass': 'btn btn-default',
            "commands": [
              "${id}",
              "attendance"
            ]
          }
    }
  ],
  'criteria': [
    ],
  'searches': [  ],
  'paths' : [  ]
};
