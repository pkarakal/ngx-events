import {Component, Injector, Input, OnInit} from '@angular/core';
import {EventsService, Model} from '../../events.service';
import {DateFormatter} from '../../advanced-date.formatter';
import {Router} from '@angular/router';

@Component({
  selector: 'universis-upcoming-events',
  templateUrl: './upcoming-events.component.html'
})
export class UpcomingEventsComponent implements OnInit {

  public events: Array<any>;
  public isLoading: boolean;
  @Input() model: Model;
  public _model = Model.Instructor;

  constructor(private _eventsService: EventsService,
              private _injector: Injector) {
  }

  async ngOnInit() {
    this.isLoading = true;
    this.events = await this._eventsService.getUpcomingEvents(this.model);
    this.events.forEach(x => {
      x = {...x, color: DateFormatter.stringToColour(x.name)};
    });
    this.isLoading = false;
  }

  goToURL(event: any) {
    if (event && event.courseClass && event.courseClass.course && event.id) {
      const router: Router = this._injector.get(Router);
      return router.navigate([
        '/courses',
        event.courseClass.course,
        event.courseClass.year,
        event.courseClass.period,
        'teachingEvents',
        event.id,
        'attendance'
      ]);
    }

  }
}
